package com.example.digiatalId;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EntityScan("com.example.digitalId.entity")
@EnableJpaRepositories("com.example.digitalId.repository")
@SpringBootApplication
@EnableSwagger2
@ComponentScan("com.example.digitalId.*")
public class DigiatalIdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigiatalIdApplication.class, args);
	}

}
