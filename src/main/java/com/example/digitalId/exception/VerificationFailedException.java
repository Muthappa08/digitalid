package com.example.digitalId.exception;

public class VerificationFailedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public VerificationFailedException() {
		super();
	}

	public VerificationFailedException(final String message) {
		super(message);
	}
}
