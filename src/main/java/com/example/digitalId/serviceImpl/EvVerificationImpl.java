package com.example.digitalId.serviceImpl;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.digitalId.constants.ApplicationConstants;
import com.example.digitalId.entity.Verification;
import com.example.digitalId.exception.VerificationFailedException;
import com.example.digitalId.repository.Verificationrepository;
import com.example.digitalId.service.EvVerification;

@Service
public class EvVerificationImpl implements EvVerification{

	@Autowired
	Verificationrepository verificationrepository;
	public Optional<Verification> verification(String firstName, String lastName, LocalDate dob, String pob) {
		Optional<Verification> verification=verificationrepository.findByFirstNameAndLastNameAndDobAndPob(firstName,lastName,dob,pob);
		
		return verification;
	}

}
