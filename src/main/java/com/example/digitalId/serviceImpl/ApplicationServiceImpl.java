package com.example.digitalId.serviceImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.digitalId.constants.ApplicationConstants;
import com.example.digitalId.dto.ApplicationDto;
import com.example.digitalId.dto.VerificationResponseDto;
import com.example.digitalId.entity.Application;
import com.example.digitalId.entity.Verification;
import com.example.digitalId.repository.ApplicationRepository;
import com.example.digitalId.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	ModelMapper mapper=new ModelMapper();
	@Autowired
	EvVerificationImpl evVerificationImpl;
	@Autowired
	ApplicationRepository applicationRepository;
	
	public VerificationResponseDto submitApplication(ApplicationDto applicationDto) throws MalformedURLException {
		VerificationResponseDto verificationResponseDto=new VerificationResponseDto();
		Optional<Verification> verification=evVerificationImpl.verification(applicationDto.getFirstName(),applicationDto.getLastName(),applicationDto.getDob(),applicationDto.getPob());
		//if(verification.)
		if(!verification.isPresent()) {
			verificationResponseDto.setApplicationStatus(ApplicationConstants.APP_STATUS_PASS);
			verificationResponseDto.setEvStatus(ApplicationConstants.EV_STATUS_FAILED);
			URL url=new URL("localhost:9001/manualSubmission");
			verificationResponseDto.setUrL(url);
			verificationResponseDto.setStatusCode(ApplicationConstants.EV_STATUS_FAILED_CODE);
		}
		Application application=new Application();
		mapper.map(applicationDto, application);
		application.setEvStatus("Pass");
		applicationRepository.save(application);
		verificationResponseDto.setApplicationStatus(ApplicationConstants.APP_STATUS_PASS);
		verificationResponseDto.setEvStatus(ApplicationConstants.EV_STATUS_PASS);
		verificationResponseDto.setStatusCode(ApplicationConstants.APPLICATION_SUCCESS_CODE);
		return verificationResponseDto;
	}

}
