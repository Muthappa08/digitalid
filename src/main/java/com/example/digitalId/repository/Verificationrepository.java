package com.example.digitalId.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.digitalId.entity.Verification;

public interface Verificationrepository extends JpaRepository<Verification, Long> {

	Optional<Verification> findByFirstNameAndLastNameAndDobAndPob(String firstName, String lastName, LocalDate dob, String pob);

}
