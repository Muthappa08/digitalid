package com.example.digitalId.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.digitalId.entity.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long>{
  
}
