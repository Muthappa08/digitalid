package com.example.digitalId.controller;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.digitalId.dto.ApplicationDto;
import com.example.digitalId.dto.VerificationResponseDto;
import com.example.digitalId.serviceImpl.ApplicationServiceImpl;

@RestController
public class ApplicationController {

	@Autowired
	ApplicationServiceImpl applicationServiceImpl;
	
	@PostMapping("/submitApplication")
	public ResponseEntity<VerificationResponseDto> submitApplication(@RequestBody ApplicationDto applicationDto) throws MalformedURLException{
		VerificationResponseDto response=applicationServiceImpl.submitApplication(applicationDto);
		return new ResponseEntity<VerificationResponseDto>(response,HttpStatus.OK);
		
	}
	/*
	 * @PostMapping("/ManualSubmission") public
	 * ResponseEntity<ManualSubmissionResponseDto> ManualSubmission(@RequestBody )
	 */
}
