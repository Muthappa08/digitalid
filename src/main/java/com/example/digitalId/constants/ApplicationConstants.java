package com.example.digitalId.constants;

import java.net.URI;

public class ApplicationConstants {

	public static final Integer APPLICATION_SUCCESS_CODE = 1000;
	public static final String APPLICATION_SUCCESS_MESSAGE = "application submitted successfully";
	
	public static final Integer EV_STATUS_FAILED_CODE = 2000;
	public static final String EV_STATUS_FAILED = "Ev status Failed";
	
	
	public static final String APP_STATUS_PASS = "Application Created";
	public static final String EV_STATUS_PASS = " EV verification Pass";
	public static final Integer APPLICATION_SUBMITTED_CODE = 3000;
	//public static final URI APP_SS = "Application Created";
}
