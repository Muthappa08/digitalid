package com.example.digitalId.dto;

import java.net.URI;
import java.net.URL;

public class VerificationResponseDto {

	private String applicationStatus;
	private String evStatus;
	private URL url;
	private Integer statusCode;
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getEvStatus() {
		return evStatus;
	}
	public void setEvStatus(String evStatus) {
		this.evStatus = evStatus;
	}
	public URL getUrL() {
		return url;
	}
	public void setUrL(URL url) {
		this.url = url;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	
	
	
}
